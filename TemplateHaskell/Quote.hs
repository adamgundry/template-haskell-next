{-# LANGUAGE Safe #-}
module TemplateHaskell.Quote
  (
        Quote (..),

        -- * Type abbreviations
        InfoQ, ExpQ, DecQ, DecsQ, ConQ, TypeQ, KindQ,
        TyLitQ, CxtQ, PredQ, DerivClauseQ, MatchQ, ClauseQ, BodyQ, GuardQ,
        StmtQ, RangeQ, SourceStrictnessQ, SourceUnpackednessQ, BangQ,
        BangTypeQ, VarBangTypeQ, StrictTypeQ, VarStrictTypeQ, FieldExpQ, PatQ,
        FieldPatQ, RuleBndrQ, TySynEqnQ, PatSynDirQ, PatSynArgsQ,
        FamilyResultSigQ, DerivStrategyQ,
   ) where

import Language.Haskell.TH.Lib
import Language.Haskell.TH.Syntax
