{- |

This module gives an abstract syntax tree for Haskell represented using
algebraic data types.

The lowercase versions (/syntax operators/) of these constructors exported by
"TemplateHaskell.Syntax.Monadic" are preferred to these constructors, since
they compose better with quotations (@[| |]@) and splices (@$( ... )@).
-}
{-# LANGUAGE Safe #-}
module TemplateHaskell.Syntax.Datatypes
    (
    -- * Expressions
        Exp(..),
        FieldExp,
        SumAlt,
        SumArity,
      -- ** Literals
        Lit(..),
        Word8,
        Bytes(..),

      -- ** Case expressions
        Match(..),
        Body(..),
        Guard(..),
      -- ** Do-notation
        Stmt(..),
      -- ** List sequence ranges
        Range(..),

    -- * Patterns
        Pat(..),
        FieldPat,

    -- * Types
        Kind,
        Type(..),
        TyLit(..),
        Cxt,
        Pred,

      -- ** Type variable binders
        TyVarBndr(..),
        Specificity(..),
        BndrVis(..),
        TyVarBndrUnit,
        TyVarBndrSpec,
        TyVarBndrVis,

    -- * Declarations
        Dec(..),
      -- ** Clauses
        Clause(..),
      -- ** Deriving clauses
        DerivClause(..),
      -- ** Functional dependencies
        FunDep(..),
      -- ** Type families
        TySynEqn(..),
        TypeFamilyHead(..),
        FamilyResultSig(..),
        InjectivityAnn(..),
      -- ** Data constructors
        Con(..),
        BangType,
        VarBangType,
        SourceUnpackedness(..),
        SourceStrictness(..),
        Bang(..),
        Strict, -- TODO: deprecate Strict type synonym
      -- ** Foreign declarations
        Foreign(..),
        Callconv(..),
        Safety(..),
      -- ** Pragmas
        Pragma(..),
        Inline(..),
        RuleMatch(..),
        Phases(..),
        RuleBndr(..),
        AnnTarget(..),
      -- ** Fixity declarations
        Fixity(..),
        FixityDirection(..),
        NamespaceSpecifier(..),
        defaultFixity,
        maxPrecedence,
      -- ** Pattern synonym declarations
        PatSynDir(..),
        PatSynArgs(..),
        PatSynType,
      -- ** Role annotations
        Role(..),
   ) where

import Data.Word
import Language.Haskell.TH.Lib
import Language.Haskell.TH.Syntax
