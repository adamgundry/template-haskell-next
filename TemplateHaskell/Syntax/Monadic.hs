{-|

Syntax operators for constructing abstract syntax trees.

These compose better with quotations (@[| |]@) and splices
(@$( ... )@) than the concrete datatypes defined in
"TemplateHaskell.Syntax.Datatypes", so they are preferred when constructing
syntax.

-}
{-# LANGUAGE Safe #-}
module TemplateHaskell.Syntax.Monadic
  (
    -- * Expressions
        dyn, varE, unboundVarE, labelE, implicitParamVarE, conE, litE, staticE,
        appE, appTypeE, uInfixE, parensE, infixE, infixApp, sectionL, sectionR,
        lamE, lam1E, lamCaseE, lamCasesE, tupE, unboxedTupE, unboxedSumE, condE,
        multiIfE, letE, caseE, appsE, listE, sigE, recConE, recUpdE, stringE,
        fieldExp, getFieldE, projectionE, typedSpliceE, typedBracketE, typeE,

      -- ** Literals
        intPrimL, wordPrimL, floatPrimL, doublePrimL, integerL, rationalL,
        charL, stringL, stringPrimL, charPrimL, bytesPrimL, mkBytes,

      -- ** Statements
        doE, mdoE, compE,
        bindS, letS, noBindS, parS, recS,

      -- ** Ranges
        fromE, fromThenE, fromToE, fromThenToE,
      -- *** Ranges with more indirection
        arithSeqE,
        fromR, fromThenR, fromToR, fromThenToR,

    -- * Patterns
        litP, varP, tupP, unboxedTupP, unboxedSumP, conP, uInfixP, parensP,
        infixP, tildeP, bangP, asP, wildP, recP,
        listP, sigP, viewP, typeP, invisP,
        fieldPat,

    -- ** Pattern Guards
        normalB, guardedB, normalG, normalGE, patG, patGE, match, clause,

    -- * Types
        forallT, forallVisT, varT, conT, appT, appKindT, arrowT, mulArrowT,
        infixT, uInfixT, promotedInfixT, promotedUInfixT,
        parensT, equalityT, listT, tupleT, unboxedTupleT, unboxedSumT,
        sigT, litT, wildCardT, promotedT, promotedTupleT, promotedNilT,
        promotedConsT, implicitParamT,
    -- ** Type literals
    numTyLit, strTyLit, charTyLit,
    -- ** Strictness
    noSourceUnpackedness, sourceNoUnpack, sourceUnpack,
    noSourceStrictness, sourceLazy, sourceStrict,
    isStrict, notStrict, unpacked,
    bang, bangType, varBangType, strictType, varStrictType,
    StrictType, VarStrictType,
    -- ** Class Contexts
    cxt, classP, equalP,
    -- ** Constructors
    normalC, recC, infixC, forallC, gadtC, recGadtC,

    -- ** Kinds
    varK, conK, tupleK, arrowK, listK, appK, starK, constraintK,

    -- *** Type variable binders
    DefaultBndrFlag(defaultBndrFlag),
    plainTV, kindedTV,
    plainInvisTV, kindedInvisTV,
    plainBndrTV, kindedBndrTV,
    specifiedSpec, inferredSpec,
    bndrReq, bndrInvis,

    -- *** Roles
    nominalR, representationalR, phantomR, inferR,

    -- * Top Level Declarations

    -- ** Data
    valD,
    funD, funD_doc,
    tySynD,
    dataD, dataD_doc,
    newtypeD, newtypeD_doc,
    typeDataD, typeDataD_doc,
    derivClause, DerivClause(..),
    stockStrategy, anyclassStrategy, newtypeStrategy,
    viaStrategy, DerivStrategy(..),

    -- ** Class
    classD, instanceD, instanceWithOverlapD, Overlap(..),
    sigD, kiSigD, standaloneDerivD, standaloneDerivWithStrategyD, defaultSigD,

    -- ** Role annotations
    roleAnnotD,

    -- ** Type Family / Data Family
    dataFamilyD, openTypeFamilyD, closedTypeFamilyD,
    dataInstD, dataInstD_doc,
    newtypeInstD, newtypeInstD_doc,
    tySynInstD,
    tySynEqn, injectivityAnn, noSig, kindSig, tyVarSig,

    -- ** Fixity
    infixLD, infixRD, infixND,

    -- ** Default declaration
    defaultD,

    -- ** Foreign Function Interface (FFI)
    cCall, stdCall, cApi, prim, javaScript,
    unsafe, safe, interruptible, forImpD,

    -- ** Functional dependencies
    funDep,

    -- ** Pragmas
    ruleVar, typedRuleVar,
    valueAnnotation, typeAnnotation, moduleAnnotation,
    pragInlD, pragSpecD, pragSpecInlD, pragSpecInstD, pragRuleD, pragAnnD,
    pragLineD, pragCompleteD,

    -- ** Pattern Synonyms
    patSynD, patSynD_doc,
    patSynSigD, unidir, implBidir, explBidir, prefixPatSyn,
    infixPatSyn, recordPatSyn,

    -- ** Implicit Parameters
    implicitParamBindD,

    -- * Documentation
    withDecDoc, withDecsDoc

   ) where

import Language.Haskell.TH.Lib
import Language.Haskell.TH.Syntax
