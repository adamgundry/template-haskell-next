-- | A splice @$(...)@ executes code in the 'Q' monad to produce some syntax,
-- which is then inserted into the module being compiled in place of the splice.
--
-- For example, given the program
--
-- > x :: Int
-- > x = $(let s = [| 2 + 2 |] in s)
--
-- GHC will check that @let s = [| 2 |] in s@ has type @'Q' 'Exp'@ (aka 'ExpQ'),
-- run the computation to produce the 'Exp' @2 + 2@, then continue as if the
-- source program contained:
--
-- > x :: Int
-- > x = 2 + 2
--
-- See "TemplateHaskell.Syntax" for different ways to construct syntax.
--
{-# LANGUAGE Safe #-}
module TemplateHaskell.Splice
  (
        -- * The monad
        Q,

        -- * Error-handling
        reportWarning,            -- :: String -> Q ()
        report,                   -- :: Bool -> String -> Q ()
        reportError,              -- :: String -> Q ()
        recover,          -- :: Q a -> Q a -> Q a
        runIO,            -- :: IO a -> Q a

        -- * Files
        getPackageRoot,
        makeRelativeToProject,
        addDependentFile,
        addTempFile,
        addForeignFilePath,
        addForeignSource,
        ForeignSrcLang,

        -- * New declaration group
        newDeclarationGroup,

        -- * Querying the current location
        location,         -- :: Q Loc
        Loc(..),
        CharPos,

        -- * Modifying the module being compiled
        addCorePlugin,
        addModFinalizer,
        addTopDecls,

        -- * State
        getQ,
        putQ,

        -- * Documentation
        getDoc,
        putDoc,
        withDecDoc,
        withDecsDoc,
        DocLoc(..),
   ) where

import Language.Haskell.TH.Lib
import Language.Haskell.TH.Syntax
