-- | This module exists to work nicely with the QualifiedDo
-- extension.
--
-- @
-- import qualified TemplateHaskell.Typed.CodeDo as Code
--
-- myExample :: Monad m => Code m a -> Code m a -> Code m a
-- myExample opt1 opt2 =
--   Code.do
--    x <- someSideEffect               -- This one is of type `M Bool`
--    if x then opt1 else opt2
-- @
module TemplateHaskell.Typed.CodeDo((>>=), (>>)) where

import Language.Haskell.TH.CodeDo
import Prelude()
