{-# LANGUAGE Safe #-}
module TemplateHaskell.Typed.Unsafe
  (
    unsafeCodeCoerce
  ) where

import Language.Haskell.TH.Syntax
