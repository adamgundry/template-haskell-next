{-|

Typed Template Haskell

-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE Safe #-}
module TemplateHaskell.Typed
  (
    -- * Code
        Code(..),
        CodeQ,
        unTypeCode,
        prettyCode,

    -- * Code combinators
        hoistCode,
        bindCode,
        bindCode_,
        joinCode,

    -- * Typed expressions
        TExp,
        unType,
        TExpQ,
        liftCode,
        liftTyped,
  ) where

import Language.Haskell.TH.Lib
import Language.Haskell.TH.Syntax
import TemplateHaskell.Pretty


-- | Pretty-print the expression contained within 'Code'.
prettyCode :: forall a m . Quote m => Code m a -> m String
prettyCode = fmap pprint . unTypeCode
