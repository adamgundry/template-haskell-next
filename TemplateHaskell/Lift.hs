{-# LANGUAGE Safe #-}
module TemplateHaskell.Lift
  (
    Lift(..)

    -- * Generic Lift implementations
  , liftData
  , dataToExpQ
  , dataToPatQ
  ) where

import Language.Haskell.TH.Syntax
