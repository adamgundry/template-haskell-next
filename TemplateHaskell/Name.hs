{-# LANGUAGE Safe #-}
module TemplateHaskell.Name
  (
        -- * Name types
        Name, NameSpace,        -- Abstract
        -- * Constructing names
        mkName,         -- :: String -> Name
        newName,        -- :: Quote m => String -> m Name
        -- * Deconstructing names
        nameBase,       -- :: Name -> String
        nameModule,     -- :: Name -> Maybe String
        namePackage,    -- :: Name -> Maybe String
        nameSpace,      -- :: Name -> Maybe NameSpace
  ) where

import Language.Haskell.TH.Syntax
