{-# LANGUAGE Safe #-}
-- | Language extensions known to GHC
module TemplateHaskell.LanguageExtensions
    ( extsEnabled,
      isExtEnabled,
      Extension(..)
    ) where

import Language.Haskell.TH.Syntax
