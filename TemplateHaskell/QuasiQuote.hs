{-# LANGUAGE Safe #-}
{- |
Description : Quasi-quoting support for Template Haskell

Template Haskell supports quasiquoting, which permits users to construct
program fragments by directly writing concrete syntax.  A quasiquoter is
essentially a function with takes a string to a Template Haskell AST.
This module defines the 'QuasiQuoter' datatype, which specifies a
quasiquoter @q@ which can be invoked using the syntax
@[q| ... string to parse ... |]@ when the @QuasiQuotes@ language
extension is enabled, and some utility functions for manipulating
quasiquoters.  Nota bene: this package does not define any parsers,
that is up to you.
-}
module TemplateHaskell.QuasiQuote
  ( QuasiQuoter
  , defaultQuasiQuoter
  , quoteExp
  , quotePat
  , quoteType
  , quoteDec

  -- * Transforming quasiquoters
  , quoteFile
  ) where

import GHC.Internal.TH.Syntax
import GHC.Internal.TH.Quote
import Language.Haskell.TH.Quote

-- TODO: callstacks etc?
defaultQuasiQuoter :: QuasiQuoter
defaultQuasiQuoter =
    QuasiQuoter
        { quoteExp  = oops "an expression"
        , quotePat  = oops "a pattern"
        , quoteType = oops "a type"
        , quoteDec  = oops "a declaration"
        }
  where
    oops :: a -> String -> Q b
    oops _ s = fail $ "This quasiquoter cannot be used in " ++ s ++ " context"
