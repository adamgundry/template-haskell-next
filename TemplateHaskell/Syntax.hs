{- |

There are various ways of constructing syntax using the @template-haskell@ API:

 - Untyped quotes (see "TemplateHaskell.Quote")

 - Typed quotes (see "TemplateHaskell.Typed") and 'TemplateHaskell.Typed.unTypeCode'

 - Using the AST datatypes (see "TemplateHaskell.Syntax.Datatypes")

 - Using the syntax operators (see "TemplateHaskell.Syntax.Monadic")

-}

{-# LANGUAGE Safe #-}
module TemplateHaskell.Syntax
  ( -- * AST construction using syntax operators
    module TemplateHaskell.Syntax.Monadic,

    -- * AST represented as plain datatypes.
    module TemplateHaskell.Syntax.Datatypes,
  ) where

import TemplateHaskell.Syntax.Datatypes (Exp, FieldExp, SumAlt, SumArity, Lit, Word8, Bytes, Match, Body, Guard, Stmt, Range, Pat, FieldPat, Kind, Type, TyLit, Cxt, Pred, TyVarBndr, Specificity, BndrVis, TyVarBndrUnit, TyVarBndrSpec, TyVarBndrVis, Dec, Clause, DerivClause, FunDep, TySynEqn, TypeFamilyHead, FamilyResultSig, InjectivityAnn, Con, BangType, VarBangType, SourceUnpackedness, SourceStrictness, Bang, Strict, Foreign, Callconv, Safety, Pragma, Inline, RuleMatch, Phases, RuleBndr, AnnTarget, Fixity, FixityDirection, NamespaceSpecifier, PatSynDir, PatSynArgs, PatSynType, Role)
import TemplateHaskell.Syntax.Monadic
