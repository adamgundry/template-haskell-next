-- | Querying the compiler
--
-- See also "TemplateHaskell.LanguageExtensions" for querying language extensions.
--
{-# LANGUAGE Safe #-}
module TemplateHaskell.Reify
  (     -- * Reify
        reify,            -- :: Name -> Q Info
        Info(..),
        InstanceDec,
        ParentName,
        Arity,
        Unlifted,
        -- * Module lookup
        thisModule,
        reifyModule,
        Module(..),
        PkgName(..),
        ModName(..),
        ModuleInfo(..),
        -- * Name lookup
        lookupTypeName,  -- :: String -> Q (Maybe Name)
        lookupValueName, -- :: String -> Q (Maybe Name)
        -- * Fixity lookup
        reifyFixity,
        -- * Type lookup
        reifyType,
        -- * Instance lookup
        reifyInstances,
        isInstance,
        -- * Roles lookup
        reifyRoles,
        -- * Annotation lookup
        reifyAnnotations,
        AnnLookup(..),
        -- * Constructor strictness lookup
        reifyConStrictness,
        DecidedStrictness(..),
   ) where

import Language.Haskell.TH.Lib
import Language.Haskell.TH.Syntax
