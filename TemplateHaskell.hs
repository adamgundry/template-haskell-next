{- | The public face of Template Haskell

-}
module TemplateHaskell
  ( module TemplateHaskell.Name
  , module TemplateHaskell.Quote
  , module TemplateHaskell.Reify
  , module TemplateHaskell.Splice
  , module TemplateHaskell.Syntax

    -- * Typed Template Haskell
  , module TemplateHaskell.Typed
  ) where

import TemplateHaskell.Name
import TemplateHaskell.Quote
import TemplateHaskell.Reify
import TemplateHaskell.Splice
import TemplateHaskell.Syntax
import TemplateHaskell.Typed (Code, unTypeCode)
