# template-haskell-next

This is a prototype for a new version of the `template-haskell` package. Key ideas:

 * The package should compile with a range of `template-haskell`/`ghc-internal` versions (how far back is yet to be determined).

 * The old `Language.Haskell.TH` API should remain available, frozen at a particular version, with users encouraged to migrate to a new module hierarchy (currently under `TemplateHaskell`).

 * Migration to the new hierarchy should not be unreasonably difficult, but we may introduce small consistency changes that are easier to make now rather than later.

 * Abstract datatypes and views will be introduced so that the package can continue to maintain cross-version compatibility.

 * When ready, we plan to publish `template-haskell-next` for wider user feedback, on the basis that it is initially experimental. Hopefully in the future it may be incorporated into a future version of `template-haskell`.
